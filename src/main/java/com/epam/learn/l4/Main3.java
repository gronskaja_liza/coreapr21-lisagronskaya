package com.epam.learn.l4;

public class Main3 {
    public static void main(String[] args) {
        //exp1 - init, exp2 - conditions, exp3 - what happens after
//        for (int i = 1; i < 5; i++) {
//            System.out.println(i);
//        }
        int i = 5;
        for (;i < 10; ) {
            System.out.println(++i);
        }
    }
}
