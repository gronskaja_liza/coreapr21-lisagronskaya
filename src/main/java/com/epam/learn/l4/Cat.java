package com.epam.learn.l4;

public class Cat extends Animal {
    private String name;
    private int age;

    public Cat(String name) {
       this.name = name;
    }

    public String toString() {
        return name;
    }
}

