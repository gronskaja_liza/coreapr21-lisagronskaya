package com.epam.learn.l3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class Main {
    private static boolean isTrue = true;
//    if:

    public static void main(String[] args) {
        // reader - позволяет считывать  консоли
//        String line = null;
//        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
//            //readLine - считать линию с консоли
//            line = reader.readLine();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        int finalResult = Integer.parseInt(Objects.requireNonNull(line));

        //в такой конструкции если приходит
//        if (finalResult == 1) {
//            System.out.println("green");
//        } else if (finalResult ==2) {
//            System.out.println("yellow");
//        } else {
//            System.out.println("red");
//        }
//    }
        Colours colours = Colours.RED;
        if (getColourByTrololo(colours) == Colours.GREEN.name()) {
            System.out.println("GREEN");
        } else if (getColourByTrololo(colours) == Colours.YELLOW.name()) {
            System.out.println("YELLOW");
        } else if (getColourByTrololo(colours) == Colours.RED.name()){
            System.out.println("RED");
        } else new RuntimeException();
    }
    private static String getColourByTrololo(Colours colours) {
        return colours.toString();
    }
}
