package com.epam.learn.l7;

public class Dog extends Animal {
    public Dog(String name) {
        super(name + "2");
    }

    public static void main(String[] args) {
        Dog dog = new Dog("Sharik");
        System.out.println(dog.getName());
    }
}
